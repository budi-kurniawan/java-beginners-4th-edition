package app23;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

public class DesktopDemo {
    private static Desktop desktop;
    
    private static void constructGUI() {
        JMenuItem openItem;
        JMenuItem editItem;
        JMenuItem printItem;
        JMenuItem browseToItem;
        JMenuItem mailToItem;
        JMenu fileMenu = new JMenu("File");
        JMenu mailMenu = new JMenu("Email");
        JMenu browseMenu = new JMenu("Browser");

        openItem = new JMenuItem("Open");
        openItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser();
                if(chooser.showOpenDialog(null) == 
                        JFileChooser.APPROVE_OPTION) {
                    try {
                        desktop.open(chooser.getSelectedFile().
                                getAbsoluteFile());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }    
            }
        });
        fileMenu.add(openItem);

        editItem = new JMenuItem("Edit");
        editItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser();
                if(chooser.showOpenDialog(null) == 
                        JFileChooser.APPROVE_OPTION) {
                    try {

                        desktop.edit(chooser.getSelectedFile()
                                .getAbsoluteFile());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }    
            }
        });
        fileMenu.add(editItem);

        printItem = new JMenuItem("Print");
        printItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser();
                if(chooser.showOpenDialog(null) == 
                        JFileChooser.APPROVE_OPTION) {
                    try {
                        desktop.print(chooser.getSelectedFile().
                            getAbsoluteFile());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }    
            }
        });
        fileMenu.add(printItem);

        browseToItem = new JMenuItem("Go to www.yahoo.com");
        browseToItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    URI browseURI = new URI("www.yahoo.com");
                    desktop.browse(browseURI);
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }
        });
        browseMenu.add(browseToItem);
        
        mailToItem = new JMenuItem("Email to sun@sun.com");
        mailToItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                   URI mailURI = new 
                           URI("mailto:support@mycompany.com");
                   desktop.mail(mailURI);
                } catch (Exception ex) {
                    System.out.println(ex.getMessage());
                }
            }
        });
        mailMenu.add(mailToItem);
        
        JMenuBar jMenuBar = new JMenuBar();
        jMenuBar.add(fileMenu);
        jMenuBar.add(browseMenu);
        jMenuBar.add(mailMenu);

        JFrame frame = new JFrame();
        frame.setTitle("Desktop Helper Applications");
        frame.setSize(300, 100);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setJMenuBar(jMenuBar);
        frame.setVisible(true);
    }
    
    
    public static void main(String[] args) {
        if (Desktop.isDesktopSupported()) {
            desktop = Desktop.getDesktop();
        } else {
            System.out.println("Desktop class is not supported");
            System.exit(1);
        }
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                constructGUI();
            }
        });
    }
}