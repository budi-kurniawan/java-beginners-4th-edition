package app23;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;

public class WindowListenerDemo1 extends JFrame {
    public WindowListenerDemo1(String title) {
        super(title);
        this.addWindowListener(new WindowAdapter() {
            public void windowIconified(WindowEvent e) {
                WindowListenerDemo1.this.setState(Frame.NORMAL);
            }
        });
    }

    private static void constructGUI() {
        JFrame.setDefaultLookAndFeelDecorated(true);
        WindowListenerDemo1 frame = 
                new WindowListenerDemo1("WindowEventDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(100, 100);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                constructGUI();
            }
        });
    }
}