package app24;
import java.awt.BorderLayout;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class MyJApplet extends JApplet {
    private static final long serialVersionUID = 1L;

    public void start() {
        this.setLayout(new BorderLayout());
        JLabel label1 = new JLabel("Registration Form");
        label1.setHorizontalAlignment(JLabel.CENTER);
        this.add(label1, BorderLayout.NORTH);
        JLabel label2 = new JLabel("Name:");
        this.add(label2, BorderLayout.WEST);
        JTextField textField = new JTextField("<your name>");
        this.add(textField, BorderLayout.CENTER);
        JButton button1 = new JButton("Register");
        this.add(button1, BorderLayout.EAST);
        JButton button2 = new JButton("Clear Form");
        this.add(button2, BorderLayout.SOUTH);
    }
}