package app22;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class JDialogDemo extends JFrame {
    AddressDialog dialog = new AddressDialog(this, false);
    public JDialogDemo(String title) {
        super(title);
        init();
    }
    public JDialogDemo() {
        super();
        init();
    }
    private void init() {
        this.getContentPane().setLayout(new FlowLayout());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        AddressDialog dialog = new AddressDialog(this, false);
        JButton button = new JButton("Show Dialog");
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                displayDialog();
            }
        });
        this.getContentPane().add(button);
    }

    private void displayDialog() {
        dialog.setSize(250, 120);
        dialog.setVisible(true);
    }
    private static void constructGUI() {
        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);
        JDialogDemo frame = new JDialogDemo();
        frame.pack();
        frame.setVisible(true);
    }
        
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
          public void run() {
            constructGUI();
          }
        });
    }
}